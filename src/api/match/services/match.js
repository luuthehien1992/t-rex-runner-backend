'use strict';

/**
 * match service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::match.match', ({ strapi }) => ({
    async create(uid) {
        const user = await strapi.query('plugin::users-permissions.user').findOne({
            where: { id: uid },
        });
        let match = await super.create({
            data: {
                user: user,
            },
        });
        return {
            match_id: match.id,
            highest_score: user.highest_score,
        }
    },

    async endMatch(uid, match_id, score) {
        let match = await strapi.db.query('api::match.match').findOne({
            select: ['*'],
            where: {
                id: match_id
            },
            populate: ['user']
        });

        if (!match) {
            throw Error("Match is not exist!");
        }
        if (match.is_end) {
            throw Error("Match is end before!")
        }
        if (uid != match.user.id) {
            throw Error("Wrong user!");
        }

        console.log("first match: ");
        console.log(match);

        await super.update(match_id, {
            data: {
                is_end: true,
                score: score,
            },
        });


        let user = await strapi.query('plugin::users-permissions.user').findOne({
            where: { id: uid },
        });

        console.log("Not Change database: " + user.highest_score + " " + score);
        console.log(typeof (user.highest_score));
        console.log(typeof (score));


        if (user && Number(user.highest_score) < Number(score)) {
            console.log("Chane database: " + user.highest_score + " " + score);
            await strapi.query('plugin::users-permissions.user').update({
                data: {
                    highest_score: score,
                },
                where: { id: uid },
            });
        }

        console.log("user: ");
        console.log(user);

        user = await strapi.query('plugin::users-permissions.user').findOne({
            where: { id: uid },
        });
        //todo

        return {
            match_id: match_id,
            score: score,
            user_id: user.id,
            highest: user.highest_score,
        }
    },
}));

