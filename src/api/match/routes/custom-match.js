module.exports = {
  routes: [
    {
      method: 'POST',
      path: '/match/:match_id/:score/end-match',
      handler: 'api::match.match.endMatch',
      config: {
        auth: false,
      },
    }
  ]
}
