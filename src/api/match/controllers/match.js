'use strict';

/**
 * match controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::match.match', ({ strapi }) => ({
    async create(context) {
        const uid = 1;
        return await strapi.service('api::match.match').create(uid);
    },
    async endMatch(context) {
        const uid = 1;
        const { match_id, score } = context.params;
        console.log("context: ");
        console.log(context.params);
        return await strapi.service('api::match.match').endMatch(uid, match_id, score);
    },

}));

